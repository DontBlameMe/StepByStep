package dev.dontblameme.stepbystep.lock

import dev.dontblameme.stepbystep.ui.windows.DialogWindow
import java.io.File
import java.nio.file.Path
import kotlin.system.exitProcess

class LockManager(folderPath: Path) {

    private val lockFile = File(folderPath.toString().plus("/already_running.lock"))

    fun setupLock() {
        if(lockFile.exists()) {
            DialogWindow(DialogWindow.DialogType.ERROR, "The application is already running. If you suspect an error, please remove the lock file at \"${lockFile.path}\".")
            exitProcess(1)
        } else
            lockFile.createNewFile()
    }

    fun unlock() {
        if(!lockFile.delete())
            lockFile.deleteOnExit()
    }
}