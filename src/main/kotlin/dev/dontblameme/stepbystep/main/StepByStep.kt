package dev.dontblameme.stepbystep.main

import dev.dontblameme.stepbystep.firstlaunch.FirstLaunchManager
import dev.dontblameme.stepbystep.lock.LockManager
import dev.dontblameme.stepbystep.mouse.MouseListener
import dev.dontblameme.stepbystep.ui.windows.DialogWindow
import org.jnativehook.GlobalScreen
import org.jnativehook.NativeHookException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.system.exitProcess

fun main() {
    val folderPath: Path = FileSystems.getDefault().getPath(
        System.getProperty("user.home") +
                FileSystems.getDefault().separator +
                "Documents" +
                FileSystems.getDefault().separator +
                "StepByStep")
    val firstLaunchManager = FirstLaunchManager(folderPath)
    val lockManager = LockManager(folderPath)

    if(Files.notExists(folderPath))
        Files.createDirectories(folderPath)

    if(firstLaunchManager.initializeIfNeeded())
        exitProcess(0)

    lockManager.setupLock()

    Logger.getLogger(GlobalScreen::class.java.`package`.name).level = Level.OFF

    try {
        GlobalScreen.registerNativeHook()
    } catch(e: NativeHookException) {
        DialogWindow(DialogWindow.DialogType.ERROR, "An issue occurred while registering the native hook. The program must exit. Error Message: ${e.message}")
        exitProcess(1)
    }

    GlobalScreen.addNativeMouseListener(MouseListener(folderPath, lockManager))
}