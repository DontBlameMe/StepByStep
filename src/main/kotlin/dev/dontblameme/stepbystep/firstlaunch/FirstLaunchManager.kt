package dev.dontblameme.stepbystep.firstlaunch

import dev.dontblameme.stepbystep.ui.windows.FirstTimeTutorialWindow
import java.io.File
import java.nio.file.Path

class FirstLaunchManager(folderPath: Path) {

    private val initializationFile = File(folderPath.toString().plus("/initialization.lock"))

    fun initializeIfNeeded(): Boolean {
        val fileExists = initializationFile.exists()

        if(!fileExists) {
            initializationFile.createNewFile()
            FirstTimeTutorialWindow()
        }

        return !fileExists
    }
}
