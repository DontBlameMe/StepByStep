package dev.dontblameme.stepbystep.pdf

import dev.dontblameme.stepbystep.capture.Capture
import dev.dontblameme.stepbystep.capture.CaptureManager
import dev.dontblameme.stepbystep.lock.LockManager
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents
import dev.dontblameme.stepbystep.ui.windows.DialogWindow
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDType0Font
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import org.apache.pdfbox.pdmodel.graphics.state.RenderingMode
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.nio.file.Path
import javax.imageio.ImageIO
import kotlin.system.exitProcess

class PdfAgent {
    fun convertToPdf(captureManager: CaptureManager, imagePath: Path, formattedTime: String, lockManager: LockManager) {
        if(captureManager.getCaptures().isEmpty()) {
            exitRoutine(captureManager, lockManager)
            DialogWindow(DialogWindow.DialogType.WARNING, "Every step page has been omitted. Consequently, no PDF will be generated.")
            exitProcess(0)
        }

        val document = PDDocument()

        addStartPage(document, captureManager.description)

        for(i in 0 until captureManager.getCaptures().size) {
            val capture = captureManager.getCaptures()[i]

            val page = PDPage(PDRectangle.A4)

            document.addPage(page)

            val contentStream = PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true)
            val imageObject = createImageObject(document, capture.editedImage!!)
            val font = PDType0Font.load(document, this::class.java.classLoader.getResourceAsStream("ccl.ttf"))

            contentStream.setNonStrokingColor(DefaultComponents.backgroundColor)
            contentStream.addRect(0f, 0f, page.cropBox.width, page.cropBox.height)
            contentStream.fill()

            addHeader(contentStream, page, font, i+1, captureManager.getCaptures().size)
            addText(contentStream, page, font, capture)
            addImage(contentStream, imageObject, page)

            contentStream.close()

        }

        document.save("${imagePath}/$formattedTime.pdf")
        document.close()

        exitRoutine(captureManager, lockManager)
        DialogWindow(DialogWindow.DialogType.SUCCESS, "The Step By Step tutorial has been successfully generated. You can find the PDF at: \"$imagePath/$formattedTime.pdf\"")
        exitProcess(0)
    }

    private fun exitRoutine(captureManager: CaptureManager, lockManager: LockManager) {
        captureManager.deleteAllCaptures()
        lockManager.unlock()
    }

    private fun createImageObject(document: PDDocument, editedImage: BufferedImage): PDImageXObject {
        val byteArrayOutput = ByteArrayOutputStream()

        ImageIO.write(editedImage, "png", byteArrayOutput)

        val byteArray = byteArrayOutput.toByteArray()

        return PDImageXObject.createFromByteArray(document, byteArray, null)
    }

    private fun addStartPage(document: PDDocument, description: String) {
        val page = PDPage(PDRectangle.A4)

        document.addPage(page)

        val contentStream = PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true)
        val font = PDType0Font.load(document, this::class.java.classLoader.getResourceAsStream("ccl.ttf"))

        contentStream.setNonStrokingColor(DefaultComponents.backgroundColor)
        contentStream.addRect(0f, 0f, page.cropBox.width, page.cropBox.height)
        contentStream.fill()

        contentStream.beginText()
        contentStream.setRenderingMode(RenderingMode.STROKE)
        contentStream.newLineAtOffset(calculateCenterOffset("Step By Step", 45f, page.artBox.width, font), page.artBox.height - 300f)
        contentStream.setFont(font, 45f)
        contentStream.setStrokingColor(DefaultComponents.highlightColor)
        contentStream.showText("Step By Step")
        contentStream.endText()

        contentStream.beginText()
        contentStream.setRenderingMode(RenderingMode.FILL)
        contentStream.newLineAtOffset(calculateCenterOffset(description, 10f, page.artBox.width, font), page.artBox.height - 350f)
        contentStream.setFont(font, 10f)
        contentStream.setNonStrokingColor(DefaultComponents.foregroundColor)
        contentStream.showText(description)
        contentStream.endText()

        contentStream.close()
    }

    private fun calculateCenterOffset(text: String, fontSize: Float, pageWidth: Float, font: PDType0Font) = (pageWidth - font.getStringWidth(text) / 1000 * fontSize) / 2

    private fun addHeader(contentStream: PDPageContentStream, page: PDPage, font: PDType0Font, currentStep: Int, totalSteps: Int) {
        val progressText = "Step $currentStep/$totalSteps"

        contentStream.beginText()
        contentStream.newLineAtOffset(calculateCenterOffset(progressText, 24f, page.artBox.width, font), page.artBox.height - 50f)
        contentStream.setFont(font, 24f)
        contentStream.setNonStrokingColor(DefaultComponents.highlightColor)
        contentStream.showText(progressText)
        contentStream.endText()
    }

    private fun addImage(contentStream: PDPageContentStream, imageObject: PDImageXObject, page: PDPage) {
        val x = 50f
        val y = page.artBox.height - 150f - imageObject.image.height
        val width = imageObject.image.width.toFloat()
        val height = imageObject.image.height.toFloat()

        val borderWidth = 2f

        // Border
        contentStream.setLineWidth(borderWidth)
        contentStream.setStrokingColor(DefaultComponents.highlightColor)
        contentStream.addRect(x, y, width, height)
        contentStream.stroke()

        // Image
        contentStream.drawImage(imageObject, x, y)

    }

    private fun addText(contentStream: PDPageContentStream, page: PDPage, font: PDType0Font, capture: Capture) {
        contentStream.beginText()
        contentStream.newLineAtOffset(calculateCenterOffset(capture.text, 10f, page.artBox.width, font), page.artBox.height - 100f)

        contentStream.setNonStrokingColor(DefaultComponents.foregroundColor)
        contentStream.setFont(font, 10f)
        contentStream.showText(capture.text)

        contentStream.endText()
    }
}