package dev.dontblameme.stepbystep.mouse

import dev.dontblameme.stepbystep.capture.Capture
import dev.dontblameme.stepbystep.capture.CaptureManager
import dev.dontblameme.stepbystep.capture.CaptureUtils
import dev.dontblameme.stepbystep.capture.MouseButton
import dev.dontblameme.stepbystep.lock.LockManager
import dev.dontblameme.stepbystep.pdf.PdfAgent
import dev.dontblameme.stepbystep.ui.windows.DescriptionModifyWindow
import dev.dontblameme.stepbystep.ui.windows.DialogWindow
import dev.dontblameme.stepbystep.ui.windows.StepModifyWindow
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseListener
import java.io.File
import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.imageio.ImageIO
import kotlin.system.exitProcess

class MouseListener(private val folderPath: Path, private val lockManager: LockManager): NativeMouseListener {
    private val captureUtils = CaptureUtils()
    private val captureManager = CaptureManager()
    private var shouldListen = true

    override fun nativeMouseClicked(event: NativeMouseEvent) {
        return
    }

    override fun nativeMousePressed(event: NativeMouseEvent) {
        if(!shouldListen)
            return

        val imageCapture = captureUtils.captureAreaAroundCursor(500, 500)
        val buttonType = when(event.button) {
            1 -> MouseButton.LEFT
            2 -> {
                val tutorialName = "StepByStep_" +LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd_MM_yyyy_HH:mm:ss"))

                shouldListen = false
                // Let user insert a Description of the tutorial
                DescriptionModifyWindow(captureManager)
                // Let user modify each step
                captureManager.getCaptures().forEach { capture ->
                    StepModifyWindow(capture)
                }
                // Create PDF
                PdfAgent().convertToPdf(captureManager, folderPath, tutorialName, lockManager)
                exitProcess(0)
            }
            3 -> MouseButton.RIGHT
            else -> {
                DialogWindow(DialogWindow.DialogType.ERROR, "The provided click type is unsupported and/or invalid. The program must exit. Click Type: $event.button")
                exitProcess(1)
            }
        }
        val imagePath = File(folderPath.toString().plus("/${UUID.randomUUID()}.png"))
        val capture = Capture(imagePath.toPath(), buttonType, "Please ${buttonType.name.lowercase()}-click on the location shown in the image.")

        captureManager.addCapture(capture)
        ImageIO.write(imageCapture, "png", imagePath)
    }

    override fun nativeMouseReleased(event: NativeMouseEvent) {
        return
    }
}