package dev.dontblameme.stepbystep.ui.tools

import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.geom.RoundRectangle2D
import java.awt.image.BufferedImage
import javax.swing.ImageIcon
import javax.swing.JLabel
import javax.swing.SwingUtilities

class DrawEraseImageLabel(imagePath: String) : JLabel() {
    private val imageIcon = ImageIcon(imagePath)
    private var clickRadius = 10
    private val clickedPoints = mutableMapOf<Point, Int>()

    init {
        icon = imageIcon

        addMouseListener(object: MouseAdapter() {
            override fun mouseClicked(e: MouseEvent) {
                if(SwingUtilities.isLeftMouseButton(e))
                    clickedPoints[e.point] = clickRadius
                else if(SwingUtilities.isRightMouseButton(e))
                    removeCircleAt(e.point)

                repaint()
            }
        })

        addMouseWheelListener { e ->
            clickRadius = if (e.wheelRotation > 0)
                clickRadius.minus(1).coerceAtLeast(1)
            else
                clickRadius.plus(1).coerceAtMost(50)
        }

        addMouseMotionListener(object : MouseAdapter() {
            override fun mouseDragged(e: MouseEvent) {
                if(SwingUtilities.isLeftMouseButton(e))
                    clickedPoints[e.point] = clickRadius
                else if(SwingUtilities.isRightMouseButton(e))
                    removeCircleAt(e.point)

                repaint()
            }
        })
    }

    private fun removeCircleAt(point: Point) {
        val iterator = clickedPoints.iterator()

        while(iterator.hasNext()) {
            val (p, radius) = iterator.next()

            if(isPointInCircle(point, p, radius)) iterator.remove()
        }
    }

    private fun isPointInCircle(testPoint: Point, circleCenter: Point, radius: Int): Boolean {
        val dx = testPoint.x - circleCenter.x
        val dy = testPoint.y - circleCenter.y

        return dx * dx + dy * dy <= radius * radius
    }

    override fun paintComponent(g: Graphics) {
        val g2d = g.create() as Graphics2D

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)

        g2d.color = DefaultComponents.highlightColor

        val roundRect = RoundRectangle2D.Float(0f, 0f, width.toFloat(), height.toFloat(), 15f, 15f)

        g2d.clip = roundRect

        super.paintComponent(g2d)

        clickedPoints.forEach { (point, radius) ->
            val x = point.x - radius
            val y = point.y - radius
            val diameter = 2 * radius

            g2d.fillOval(x, y, diameter, diameter)
        }

        g2d.stroke = BasicStroke(3f)
        g2d.color = DefaultComponents.highlightColor
        g2d.drawRoundRect(0, 0, width - 1, height - 1, 15, 15)

        g2d.dispose()
    }

    fun getEditedImage(): BufferedImage {
        val bufferedImage = BufferedImage(icon.iconWidth, icon.iconHeight, BufferedImage.TYPE_INT_ARGB)
        val g = bufferedImage.createGraphics()

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)

        icon.paintIcon(this, g, 0, 0)
        g.dispose()

        val g2d = bufferedImage.createGraphics()

        g2d.color = DefaultComponents.highlightColor

        clickedPoints.forEach { (point, radius) ->
            val x = point.x - radius
            val y = point.y - radius
            val diameter = 2 * radius

            g2d.fillOval(x, y, diameter, diameter)
        }

        g2d.dispose()

        return bufferedImage
    }
}
