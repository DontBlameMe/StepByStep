package dev.dontblameme.stepbystep.ui.tools

import java.awt.*
import java.awt.geom.RoundRectangle2D
import javax.swing.border.Border

class RoundBorder(private val cornerRadius: Int, private val backgroundColor: Color): Border {
    override fun getBorderInsets(c: Component): Insets {
        return Insets(cornerRadius + 1, cornerRadius + 1, cornerRadius + 2, cornerRadius)
    }

    override fun isBorderOpaque(): Boolean {
        return true
    }

    override fun paintBorder(c: Component, g: Graphics, x: Int, y: Int, width: Int, height: Int) {
        val g2d = g as Graphics2D

        g2d.stroke = BasicStroke(4f)
        g2d.color = backgroundColor

        val clip = g2d.clip

        g2d.clip = RoundRectangle2D.Float(x.toFloat(), y.toFloat(), width.toFloat(), height.toFloat(), cornerRadius.toFloat(), cornerRadius.toFloat())

        g2d.drawRoundRect(x, y, width - 1, height - 1, cornerRadius, cornerRadius)
        g2d.clip = clip
    }
}
