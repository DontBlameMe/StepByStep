package dev.dontblameme.stepbystep.ui.tools

import java.awt.Color
import java.awt.Font
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JButton
import javax.swing.JLabel
import javax.swing.JTextField
import javax.swing.SwingConstants
import javax.swing.border.EmptyBorder
import javax.swing.border.MatteBorder
import javax.swing.text.AttributeSet
import javax.swing.text.PlainDocument

class DefaultComponents {
    companion object {
        val highlightColor: Color = Color.decode("#88c0d0")
        val foregroundColor: Color = Color.decode("#eceff4")
        val backgroundColor: Color = Color.decode("#2e3440")
        val highlightFont = Font("Arial", Font.PLAIN, 30)
        val normalFont = Font("Arial", Font.PLAIN, 20)
        val toolTipFont = Font("Arial", Font.ITALIC, 17)
        val toolTipColor: Color = Color.decode("#d8dee9")

        fun createStyledButton(text: String, customHighlightColor: Color? = null): JButton {
            return JButton(text).apply {
                font = highlightFont
                isFocusPainted = false
                foreground = foregroundColor
                background = null
                isContentAreaFilled = false
                border = RoundBorder(15, customHighlightColor ?: highlightColor)
                addMouseListener(object : MouseAdapter() {
                    override fun mouseEntered(e: MouseEvent) {
                        foreground = customHighlightColor ?: highlightColor
                        background = null
                    }

                    override fun mouseExited(e: MouseEvent) {
                        foreground = foregroundColor
                        background = null
                    }
                })
            }
        }

        fun createNormalTextLabel(text: String): JLabel {
            return JLabel(text).apply {
                font = normalFont
                foreground = foregroundColor
                isOpaque = true
                background = backgroundColor
                horizontalAlignment = SwingConstants.CENTER
                border = EmptyBorder(15, 15, 15, 15)
            }
        }

        fun createStyledLabel(text: String, customHighlightColor: Color? = null): JLabel {
            return JLabel(text).apply {
                font = highlightFont
                foreground = customHighlightColor ?: highlightColor
                background = backgroundColor
                border = null
                isOpaque = true
                horizontalAlignment = SwingConstants.CENTER
            }
        }

        fun createToolTipLabel(text: String): JLabel {
            return JLabel(text).apply {
                font = toolTipFont
                foreground = toolTipColor
                background = backgroundColor
                border = EmptyBorder(0,0,0,0)
                isOpaque = true
                horizontalAlignment = SwingConstants.CENTER
            }
        }

        fun createStyledTextField(initialText: String, columns: Int = 30, lengthLimit: Int = -1): JTextField {
            return JTextField(columns).apply {
                horizontalAlignment = SwingConstants.CENTER
                isEditable = true
                if(lengthLimit > 0)
                    document = LimitLengthDocument(lengthLimit).setInitialText(initialText)
                font = normalFont
                foreground = foregroundColor
                background = null
                caretColor = highlightColor
                border = MatteBorder(0, 0, 2, 0, highlightColor)
                selectedTextColor = foregroundColor
                selectionColor = highlightColor
            }
        }
    }

    class LimitLengthDocument(private var limit: Int) : PlainDocument() {
        override fun insertString(offset: Int, str: String?, attr: AttributeSet?) {
            if(str == null) return
            if((length + str.length) <= limit) {
                super.insertString(offset, str, attr)
            }
        }
        fun setInitialText(initialText: String): LimitLengthDocument {
            if(initialText.length <= limit)
                super.insertString(0, initialText, null)
            return this
        }
    }
}