package dev.dontblameme.stepbystep.ui.windows

import dev.dontblameme.stepbystep.ui.tools.DefaultComponents
import java.awt.*
import java.util.concurrent.CountDownLatch
import javax.swing.JFrame
import javax.swing.JPanel
import kotlin.system.exitProcess

class FirstTimeTutorialWindow {
    private val waitingLatch = CountDownLatch(1)
    private val frame = JFrame("Welcome to Step By Step")
    private val confirmButton = DefaultComponents.createStyledButton("UNDERSTOOD")

    init {
        initializeFrame()
        buildUI()
        addListeners()
        awaitLatch()
    }

    private fun initializeFrame() {
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.layout = BorderLayout()
        frame.font = DefaultComponents.normalFont
        frame.background = DefaultComponents.backgroundColor
        frame.isResizable = true
    }

    private fun buildUI() {
        val mainPanel = JPanel(GridBagLayout())
        val gbc = GridBagConstraints()

        gbc.gridx = 0
        gbc.gridy = 0

        mainPanel.background = DefaultComponents.backgroundColor
        gbc.insets = Insets(5, 5, 5, 5)

        addComponent(mainPanel, DefaultComponents.createStyledLabel("WELCOME"), gbc)
        addComponent(mainPanel, DefaultComponents.createStyledLabel("Thank you for choosing the Step By Step tutorial creator!"), gbc)
        addComponent(mainPanel, DefaultComponents.createNormalTextLabel("""
            <html>
            <p style='text-align: center;'>To utilize this application, please follow these steps:</p>
            <p style='text-align: center;'>Launch the Step By Step Application and go through your steps of clicking, as required for the creation of the desired Step By Step tutorial.</p>
            <p style='text-align: center;'>Upon completing the clicking process, press the middle mouse button. After that, you will encounter a set of questions.</p>
            <p style='text-align: center;'>After responding to all the questions and making necessary image modifications, the Step By Step tutorial PDF will be automatically generated.</p>
            </html>
            """.trimIndent()), gbc)
        addComponent(mainPanel, DefaultComponents.createStyledLabel("Click the button to exit. Upon reopening, the Step By Step recording begins."), gbc)
        addComponent(mainPanel, confirmButton, gbc)

        frame.add(mainPanel, BorderLayout.CENTER)
        frame.pack()
        frame.isVisible = true
    }

    private fun addComponent(panel: JPanel, component: Component, gbc: GridBagConstraints) {
        panel.add(component, gbc)
        gbc.gridy++
    }

    private fun addListeners() {
        confirmButton.addActionListener {
            frame.dispose()
            waitingLatch.countDown()
            exitProcess(0)
        }
    }

    private fun awaitLatch() {
        try {
            waitingLatch.await()
        } catch(e: InterruptedException) {
            DialogWindow(DialogWindow.DialogType.ERROR, "An \"InterruptedException\" occurred while waiting for the \"waitingLatch\" in the \"FirstTimeTutorialWindow\". The program must exit. Error Message: ${e.message}")
        }
    }
}