package dev.dontblameme.stepbystep.ui.windows

import dev.dontblameme.stepbystep.capture.CaptureManager
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents
import java.awt.*
import java.util.concurrent.CountDownLatch
import javax.swing.JFrame
import javax.swing.JPanel
import kotlin.system.exitProcess

class DescriptionModifyWindow(private val captureManager: CaptureManager) {
    private val waitingLatch = CountDownLatch(1)
    private val frame = JFrame("Adjust this tutorial's description to your preferences")
    private val textField = DefaultComponents.createStyledTextField("This Step By Step Tutorial explains how to ", lengthLimit = 90)
    private val confirmButton = DefaultComponents.createStyledButton("CONFIRM")

    init {
        initializeFrame()
        buildUI()
        addListeners()
        awaitLatch()
    }

    private fun initializeFrame() {
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.layout = BorderLayout()
        frame.font = DefaultComponents.normalFont
        frame.background = DefaultComponents.backgroundColor
        frame.isResizable = true
    }

    private fun buildUI() {
        val mainPanel = JPanel(GridBagLayout())
        val gbc = GridBagConstraints()

        gbc.gridx = 0
        gbc.gridy = 0

        mainPanel.background = DefaultComponents.backgroundColor
        gbc.insets = Insets(5, 5, 5, 5)

        addComponent(mainPanel, DefaultComponents.createStyledLabel("DESCRIPTION"), gbc)
        addComponent(mainPanel, DefaultComponents.createToolTipLabel("Please explain what this tutorial is about."), gbc)
        addComponent(mainPanel, textField, gbc)
        addComponent(mainPanel, confirmButton, gbc)

        frame.add(mainPanel, BorderLayout.CENTER)
        frame.pack()
        frame.isVisible = true
    }

    private fun addComponent(panel: JPanel, component: Component, gbc: GridBagConstraints) {
        panel.add(component, gbc)
        gbc.gridy++
    }

    private fun addListeners() {
        confirmButton.addActionListener {
            captureManager.description = textField.text
            frame.dispose()
            waitingLatch.countDown()
        }
    }

    private fun awaitLatch() {
        try {
            waitingLatch.await()
        } catch(e: InterruptedException) {
            DialogWindow(DialogWindow.DialogType.ERROR, "An \"InterruptedException\" occurred while waiting for the \"waitingLatch\" in the \"DescriptionModifyWindow\". The program must exit. Error Message: ${e.message}")
            exitProcess(1)
        }
    }
}