package dev.dontblameme.stepbystep.ui.windows

import dev.dontblameme.stepbystep.capture.Capture
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.backgroundColor
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.createStyledButton
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.createStyledLabel
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.createStyledTextField
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.createToolTipLabel
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.normalFont
import dev.dontblameme.stepbystep.ui.tools.DrawEraseImageLabel
import java.awt.*
import java.util.concurrent.CountDownLatch
import javax.swing.JFrame
import javax.swing.JPanel
import kotlin.system.exitProcess


class StepModifyWindow(private val capture: Capture) {
    private val waitingLatch = CountDownLatch(1)
    private val frame = JFrame("Adjust this step according to your preferences.")
    private val textField = createStyledTextField(capture.text, lengthLimit = 90)
    private val drawEraseImageLabel = DrawEraseImageLabel(capture.imagePath.toString())
    private val confirmButton = createStyledButton("INCLUDE STEP")
    private val discardButton = createStyledButton("OMIT STEP")

    init {
        initializeFrame()
        buildUI()
        addListeners()
        awaitLatch()
    }

    private fun initializeFrame() {
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.layout = BorderLayout()
        frame.font = normalFont
        frame.background = backgroundColor
        frame.isResizable = true
    }

    private fun buildUI() {
        val mainPanel = JPanel(GridBagLayout())
        val gbc = GridBagConstraints()

        gbc.gridx = 0
        gbc.gridy = 0

        mainPanel.background = backgroundColor
        gbc.insets = Insets(5, 5, 5, 5)

        addComponent(mainPanel, createStyledLabel("DESCRIPTION"), gbc)
        addComponent(mainPanel, createToolTipLabel("Please explain what to do in this step."), gbc)
        addComponent(mainPanel, textField, gbc)
        addComponent(mainPanel, createStyledLabel("IMAGE"), gbc)
        addComponent(mainPanel, createToolTipLabel("Left/Right Click - Modify Image | Scroll Up/Down - Change Brush Size"), gbc)
        addComponent(mainPanel, drawEraseImageLabel, gbc)
        addComponent(mainPanel, confirmButton, gbc)
        addComponent(mainPanel, discardButton, gbc)

        frame.add(mainPanel, BorderLayout.CENTER)
        frame.pack()
        frame.isVisible = true
    }

    private fun addComponent(panel: JPanel, component: Component, gbc: GridBagConstraints) {
        panel.add(component, gbc)
        gbc.gridy++
    }

    private fun addListeners() {
        confirmButton.addActionListener {
            capture.editedImage = drawEraseImageLabel.getEditedImage()
            capture.text = textField.text
            frame.dispose()
            waitingLatch.countDown()
        }

        discardButton.addActionListener {
            capture.markDeleted = true
            frame.dispose()
            waitingLatch.countDown()
        }
    }

    private fun awaitLatch() {
        try {
            waitingLatch.await()
        } catch(e: InterruptedException) {
            DialogWindow(DialogWindow.DialogType.ERROR, "An \"InterruptedException\" occurred while waiting for the \"waitingLatch\" in the \"ImageModifyWindow\". The program must exit. Error Message: ${e.message}")
            exitProcess(1)
        }
    }
}
