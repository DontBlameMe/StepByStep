package dev.dontblameme.stepbystep.ui.windows

import dev.dontblameme.stepbystep.ui.tools.DefaultComponents
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.backgroundColor
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.createStyledButton
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.foregroundColor
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.highlightFont
import dev.dontblameme.stepbystep.ui.tools.DefaultComponents.Companion.normalFont
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Frame
import javax.swing.JDialog
import javax.swing.JPanel

class DialogWindow(type: DialogType, text: String): JDialog(null as Frame?, text, true) {
    private val highlightColor: Color

    init {
        highlightColor = when(type) {
            DialogType.SUCCESS -> {
                Color.decode("#a3be8c")
            }

            DialogType.WARNING -> {
                Color.decode("#ebcb8b")
            }

            DialogType.ERROR -> {
                Color.decode("#bf616a")
            }
        }

        defaultCloseOperation = DISPOSE_ON_CLOSE
        layout = BorderLayout()
        background = backgroundColor
        font = normalFont
        foreground = foregroundColor

        val stateText = DefaultComponents.createStyledLabel(when(type) {
            DialogType.SUCCESS -> "SUCCESS"
            DialogType.WARNING -> "WARNING"
            DialogType.ERROR -> "ERROR"
        }, highlightColor)
        val label = DefaultComponents.createNormalTextLabel(text)
        val buttonPanel = JPanel()
        val confirmButton = createStyledButton("CONFIRM", highlightColor)

        buttonPanel.background = backgroundColor
        buttonPanel.font = highlightFont
        buttonPanel.foreground = foregroundColor

        buttonPanel.add(confirmButton)

        add(stateText, BorderLayout.NORTH)
        add(label, BorderLayout.CENTER)
        add(buttonPanel, BorderLayout.SOUTH)

        confirmButton.addActionListener {
            dispose()
        }

        pack()
        setLocationRelativeTo(owner)
        isResizable = true
        isVisible = true
    }
    enum class DialogType {
        SUCCESS,
        WARNING,
        ERROR
    }
}