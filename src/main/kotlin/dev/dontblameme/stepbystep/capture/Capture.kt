package dev.dontblameme.stepbystep.capture

import java.awt.image.BufferedImage
import java.nio.file.Path

data class Capture(val imagePath: Path, val mouseButton: MouseButton, var text: String, var editedImage: BufferedImage? = null, var markDeleted: Boolean = false)

enum class MouseButton {
    LEFT,
    RIGHT
}