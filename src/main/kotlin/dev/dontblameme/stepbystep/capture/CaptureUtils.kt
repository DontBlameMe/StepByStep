package dev.dontblameme.stepbystep.capture

import java.awt.*
import java.awt.image.BufferedImage

class CaptureUtils {

    private val robot = Robot()

    fun captureAreaAroundCursor(width: Int, height: Int): BufferedImage {
        val scaleFactor = MouseInfo.getPointerInfo().device.defaultConfiguration.bounds.width / 1920.0
        val scaledBounds = getScaledCaptureBounds(MouseInfo.getPointerInfo().location, width, height, scaleFactor)
        val capture = robot.createScreenCapture(scaledBounds)
        val scaledCapture = scaleImage(capture, 1 / scaleFactor)

        return addHighlightCircle(scaledCapture)
    }

    private fun getScaledCaptureBounds(pointerLocation: Point, width: Int, height: Int, scaleFactor: Double): Rectangle {
        val scaledWidth = width * scaleFactor.toInt()
        val scaledHeight = height * scaleFactor.toInt()

        return Rectangle(pointerLocation.x - scaledWidth / 2, pointerLocation.y - scaledHeight / 2, scaledWidth, scaledHeight)
    }

    private fun scaleImage(image: BufferedImage, scale: Double): Image {
        val scaledWidth = (image.width * scale).toInt()
        val scaledHeight = (image.height * scale).toInt()

        return image.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH)
    }

    private fun addHighlightCircle(image: Image): BufferedImage {
        val bufferedImage = BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB)
        val graphics = bufferedImage.graphics

        graphics.drawImage(image, 0, 0, null)
        drawHighlightCircle(bufferedImage.createGraphics(), bufferedImage.width / 2, bufferedImage.height / 2)
        graphics.dispose()

        return bufferedImage
    }

    private fun drawHighlightCircle(graphics: Graphics2D, centerX: Int, centerY: Int, radius: Int = 8) {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

        // Draw transparent oval
        graphics.color = Color(255, 0, 0, 100)
        graphics.fillOval(centerX - radius, centerY - radius, 2 * radius, 2 * radius)

        // Draw solid outer line
        graphics.color = Color(255, 0, 0, 255)
        graphics.stroke = BasicStroke(2.0f)
        graphics.drawOval(centerX - radius, centerY - radius, 2 * radius, 2 * radius)
    }
}