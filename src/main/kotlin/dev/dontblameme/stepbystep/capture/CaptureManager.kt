package dev.dontblameme.stepbystep.capture

import java.util.*

class CaptureManager {

    private val createdCaptures = LinkedList<Capture>()
    var description = "Undefined"

    fun getCaptures() = createdCaptures.filterNot { it.markDeleted }

    fun addCapture(capture: Capture) {
        createdCaptures.add(capture)
    }

    fun deleteAllCaptures() {
        createdCaptures.forEach { capture ->
            if(!capture.imagePath.toFile().delete())
                capture.imagePath.toFile().deleteOnExit()
        }
    }
}