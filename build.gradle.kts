plugins {
    kotlin("jvm") version "1.9.21"
}

group = "dev.dontblameme"
description = "An instrument designed for the effortless creation of Step By Step tutorials."
version = "0.1"

val jdkVersion = 21
val pdfBoxVersion = "3.0.2"
val jnativehookVersion = "2.1.0"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly("org.jetbrains.kotlin:kotlin-test")
    implementation("com.1stleg:jnativehook:$jnativehookVersion")
    implementation("org.apache.pdfbox:pdfbox:$pdfBoxVersion")
}

tasks.test {
    useJUnitPlatform()
}